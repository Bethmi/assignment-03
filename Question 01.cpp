#include <stdio.h>
int main() {
    float num;
    printf("Enter the number: ");
    scanf("%f", &num);
    if (num > 0.0)
        printf("Positive number");
    else if (num < 0.0)
        printf("Negative number");
    else
        printf("Zero");
    return 0;
}
